{- |This module only contains type and data declarations for the allowed
    data types, symbols, and the structure of the expression tree.
    Beyond show-instances, nothing is implemented here.
-}
module ExprTree (DataType(..), Signature,
                 Op (..), SOp (..), Pred (..), SPred (..), Expr (..), Stmt (..),
                 Cmd (..), CmdKind (..), replace) where

                 
import Data.List

-- |The supported data types: Bool, Int, Double, String
data DataType = DTBool | DTInt | DTDouble | DTString | DTAny deriving (Eq)

instance Show DataType where
   show DTBool = "Bool"
   show DTInt = "Integer"
   show DTDouble = "Double"
   show DTString = "String"
   show DTAny = "Any"

-- |A function's signature   
type Signature = [DataType]

-- |Allowed binary operators
data Op = Plus | Minus | Mult | Div | Mod | Pow | Rem | Or | And |
          Xor | Equiv | Concat deriving (Show, Eq)

-- |Allowed unary operators
data SOp = Neg | Not deriving (Show, Eq)

-- |Enumeration of binary predicate symbols.
data Pred = LEQ | LE | EEQ | NEQ | GE | GEQ deriving (Show, Eq)

-- |Enumeration of unary predicate symbols.             
data SPred = Positive | Negative | Zero | IsTrue | IsFalse |
             IsBool | IsInt | IsDouble | IsString deriving (Show, Eq)
             

{-|Expression tree specification. This type contains all expressions which
   can be evaluated. For statements, see @Stmt@.
-}
data Expr =
            -- atomic values
            EBool Bool |
            EInt Integer | 
            EDouble Double | 
            EString String |
            
            -- variables (possibly expression trees)
            EVar String |
            
            --casts (to bool, int, Double, string)
            EBCast Expr |
            EICast Expr |
            EFCast Expr |
            ESCast Expr |
            
            -- binary and unary operators
            EOp Expr Op Expr |
            ESOp SOp Expr |
            
            -- binary and unary predicates
            EPred Pred Expr Expr |
            ESPred SPred Expr |
            
            -- functions
            EFunc String [Expr] |
            -- function arguments
            EBound String
              deriving (Show, Eq)

-- |Recursively searches and replaces parts of the expression tree. If a node is replaced, its children are not examined.
replace :: (Expr -> (Bool, Expr)) -- ^The replacement function. The boolean value indicates whether a replacement should be performed and the Expr is the new value for the node.
        -> Expr -- ^The tree to be searched.
        -> Expr
replace f e@(EBCast i) = if repl then newE else (EBCast $ replace f i)
   where (repl, newE) = f e
replace f e@(EICast i) = if repl then newE else (EICast $ replace f i)
   where (repl, newE) = f e
replace f e@(EFCast i) = if repl then newE else (EFCast $ replace f i)
   where (repl, newE) = f e
replace f e@(ESCast i) = if repl then newE else (ESCast $ replace f i)
   where (repl, newE) = f e

replace f e@(EOp l op r) = if repl then newE else (EOp (replace f l) op (replace f r))
   where (repl, newE) = f e
replace f e@(ESOp sop l) = if repl then newE else (ESOp sop (replace f l))
   where (repl, newE) = f e
replace f e@(EPred p l r) = if repl then newE else (EPred p (replace f l) (replace f r))
   where (repl, newE) = f e
replace f e@(ESPred sp l) = if repl then newE else (ESPred sp (replace f l))
   where (repl, newE) = f e
replace f e@(EFunc fn a) = if repl then newE else (EFunc fn (map (replace f) a))
   where (repl, newE) = f e
   
replace f e = if repl then newE else e
   where (repl, newE) = f e

{- | Statement specification. This type includes variable and function definitions.
-}              
data Stmt = VarAss String Expr |
            FuncAss String ([Expr] -> Expr) Signature

-- |A command.
data Cmd = Cmd CmdKind [String] deriving (Show, Eq)
            
-- | The list of special commands that can be issued.
data CmdKind = Print | Unset | Exit | ListVars | Help deriving (Show, Eq)