-- |Contains the definition of the abstract syntax tree, together with
--  definitions for operators, data types, auxiliary information, and so forth.
module Winterheart.AST where

import Prelude hiding (EQ, LT, GT)

-- |A position in the input stream.
type Position = (Int, Int)

-- |A binary operator.
data BOP =
   -- |Addition.
   Plus |
   -- |Subtraction.
   Minus |
   -- |Multiplication.
   Mult | 
   -- |Division.
   Div |
   -- |Modulo.
   Mod |
   -- |Exponentiation.
   Exp |
   -- |Logical AND.
   And |
   -- |Logical OR.
   Or |
   -- |Logical XOR.
   Xor |
   -- |Equal.
   EQ |
   -- |Less than.
   LT |
   -- |Greater than.
   GT |
   -- |Less than or equal.
   LTE |
   -- |Greater thn or equal.
   GTE |
   -- |Not equal.
   NEQ | 
   -- |Binary AND.
   Band |
   -- |Binary OR.
   Bor |
   -- |Binary XOR.
   Bxor |
   -- |List constructor.
   Cons |
   -- |List concatenation.
   Concat
   deriving (Show, Eq, Read)

-- |A unary operator.
data UOP =
   -- |Factorial.
   Fac |
   -- |Unary minus.
   Neg |
   -- |Logical NOT.
   Not |
   -- |Binary NOT.
   Bnot 
   deriving (Show, Eq, Read)

-- |Sum type for binary and unary operators.
data OP = BOP BOP | UOP UOP

-- |A data type for a node in the AST.
--  Data type annotations are part of AuxInfo, not of AST proper.
data Datatype = 
   -- |The bottom (undefined) datatype.
   DTBottom |
   -- |A boolean.
   DTBool |
   -- |An integer.
   DTInt |
   -- |A real number (with adjustable precision).
   DTReal |
   -- |A Unicode character.
   DTChar |
   -- |A homogeneous list of arbitrary length.
   DTList Datatype |
   -- |A 2-tuple (the product type).
   DTTuple Datatype Datatype |
   -- |A curried function, with the expected input parameters as a list (the return type is the last element).
   DTFunc [Datatype] |
   -- |An unevaluated expression tree of symbols.
   DTSym Datatype |
   -- |The unit (top) data type.
   DTTop
   deriving (Show, Eq, Read)

-- |A node in the Abstract Syntax Tree (AST).
--  Contains the AST proper and auxiliary information for each node, such
--  as the position in the input and the data type.
data ASTNode auxInfo = ASTNode auxInfo (AST auxInfo) deriving (Show, Eq)

-- |An actual AST-node, without the auxiliary information of 'ASTNode'.
data AST auxInfo =
   -- |The bottom literal (undefined).
   ASTBottom |
   -- |The top literal (the "()" constructor).
   ASTTop |
   -- |Boolean literal.
   ASTBool Bool |
   -- |Positive or negative unbounded integer literal.
   ASTInteger Integer |
   -- |Double literal.
   ASTReal Double |
   -- |Unicode character literal.
   ASTChar Char |
   -- |Homogeneous list.
   ASTList [ASTNode auxInfo] |
   -- |2-tuple.
   ASTTuple (ASTNode auxInfo) (ASTNode auxInfo) |

   -- |Binary operator with its left and right operands.
   ASTOp {astop::BOP, astleft::ASTNode auxInfo, astright::ASTNode auxInfo} |
   -- |Unary operator with its operand.
   ASTUop {astuop::UOP, astleft::ASTNode auxInfo} |

   -- |A parameter in a function declaration or a lambda.
   ASTParam {astparamname::String} |

   -- |A function name (used exlusively in ASTFuncApp).
   ASTFuncName {astfuncname::String} |
   -- |A lambda (anonymous function) with 1 parameter. Multi-parameter lambdas are created by nesting.
   ASTLambda {astparam::ASTNode auxInfo, astfunclambda::ASTNode auxInfo} |
   -- |A bound variable (in a function or a lambda).
   ASTVar {astvarname::String} |

   -- |Function application of 1 argument. astfunclambda can be a lambda's code or a function name.
   ASTFuncApp {astfunclambda::ASTNode auxInfo, astfuncarg::ASTNode auxInfo} |

   -- |A function definition.
   ASTFuncDef {astfuncname::String, astfunclambda::ASTNode auxInfo}
   deriving (Show, Eq)