-- |The lexer for Winterheart's language.
--  Contains a GenTokenParser which is used by the parser
--  to tokenize the input.
module Winterheart.Syntax.Lexer 
   (identifier, parens, brackets, infixOp,
    prefixOp, postfixOp, falseLit, trueLit,
    integerLit, realLit, charLit, stringLit,
    ifLit, letLit, bottomLit, topLit, paramList,
    def, dtBool, dtInt, dtReal, dtChar)
   where

import Text.Parsec
import qualified Text.Parsec.Token as T
import Text.ParserCombinators.Parsec.Error

import Data.Functor.Identity
import Control.Applicative hiding (many, optional, (<|>))
import Data.Char
import Data.List
import Numeric
import qualified Data.List.Split as LS
import Data.Maybe
import Prelude hiding (EQ, LT, GT)

import Winterheart.AST
import Winterheart.Syntax.Operators

-- |The lexer for the language.
--  Does not have to be used directly; refer to
--  individual lexemes defined in this module (identifier, parens,...) instead.
lexer :: T.TokenParser st
lexer = T.makeTokenParser langDef

-- |The language definition for Winterheart's language.
--  Contains comment symbols, operators, reserved words,...
langDef :: T.LanguageDef st
langDef =
   T.LanguageDef {
      T.commentStart = "/*",
      T.commentEnd = "*/",
      T.commentLine = "",
      T.nestedComments = True,
      T.identStart = letter <|> char '_',
      T.identLetter = alphaNum <|> oneOf "_'",
      T.opStart = oneOf "+-*/%§^&|~<=>!:",
      T.opLetter = oneOf "+*=^-&|~",
      T.reservedNames = ["if", "let", "true", "false", "undefined", "none",
                         "bool", "int", "real", "char"],
      T.reservedOpNames = [],
      T.caseSensitive = True
   }

-- |A legal identifier that isn't a keyword.
identifier = T.identifier lexer
-- |Some token inside parentheses.
parens = T.parens lexer
-- |Some token inside square brackets.
brackets = T.brackets lexer
-- |An infix operator.
infixOp = T.operator lexer >>= checkOp infixOps
-- |A prefix operator.
prefixOp = T.operator lexer >>= checkOp prefixOps
-- |A postfix operator.
postfixOp = T.operator lexer >>= checkOp postfixOps
-- |The literal "false".
falseLit = T.symbol lexer "false"
-- |The literal "true".
trueLit = T.symbol lexer "true"
-- |Natural number.
integerLit = T.natural lexer
-- |Floating number.
realLit = T.float lexer
-- |Character literal, enclosed in single quotes.
charLit = T.charLiteral lexer
-- |String literal, enclosed in double quotes.
stringLit = T.stringLiteral lexer
-- |The literal "if".
ifLit = T.symbol lexer "if"
-- |The literal "let".
letLit = T.symbol lexer "let"
-- |The literal "undefined".
bottomLit = T.symbol lexer "undefined"
-- |The literal "top".
topLit = T.symbol lexer "top"
-- |A list of tokens, separated by commas.
paramList params = parens (T.commaSep lexer params)
-- |The literal ":=".
def = T.symbol lexer ":="
-- |The literal "bool".
dtBool = T.symbol lexer "bool"
-- |The literal "int".
dtInt = T.symbol lexer "int"
-- |The literal "real".
dtReal = T.symbol lexer "real"
-- |The literal "char".
dtChar = T.symbol lexer "char"

checkOp coll op = if not $ op `elem` coll then unexpected ("symbol '" ++ op ++ "'. That is not a valid operator.") else return op