-- |Contains syntactix information about operators: their string
-- |representations, precedences, fixities, and associativity.
module Winterheart.Syntax.Operators
   (infixOps, prefixOps, postfixOps, precedences, stringRep,
    infixTable, prefixTable, postfixTable, Associativity (..))
   where

import Prelude hiding (EQ, LT, GT)
import Data.List
import Data.Maybe
import Control.Applicative

import Winterheart.AST

-- |The list of the string representations of infix operators.
infixOps = map snd infixTable

-- |The list of the string representations of prefix operators.
prefixOps = map snd prefixTable

-- |The list of the string representations of postfix operators.
postfixOps = map snd postfixTable

-- |Indicates the associativity (left or right) of an operator.
data Associativity = LeftAss | RightAss

-- |Lists, in ASCENDING order of binding strength, the infix operators.
--  Within each level, associativity decides the exact grouping or the expressions.
precedences :: [([BOP],Associativity)]
precedences = [
  ([Or],               LeftAss),  -- logical OR
  ([Xor],              LeftAss),  -- logical XOR
  ([And],              LeftAss),  -- logical AND
  ([Bor],              LeftAss),  -- bitwise OR
  ([Bxor],             LeftAss),  -- bitwise XOR
  ([Band],             LeftAss),  -- bitwise AND
  ([EQ, NEQ],          LeftAss),  -- equality
  ([LTE, GTE, GT, LT], LeftAss),  -- Less/greater than
  ([Cons, Concat],     RightAss), -- List operations: constructor and concatenation
  ([Plus, Minus],      LeftAss),  -- 1st order arithmetical operations
  ([Mult, Div, Mod],   LeftAss),  -- 2nd order arithmetical operations
  ([Exp],              RightAss)  -- 3rd order arithmetical operations
  ]

-- |Returns the string representation of an operator.
stringRep :: OP -> String
stringRep (BOP op) = fromJust $ lookup op infixTable
stringRep (UOP op) = fromJust (lookup op prefixTable <|> lookup op postfixTable) 

-- |The list of infix operators.
infixTable :: [(BOP, String)]
infixTable = [
   (Plus, "+"),
   (Minus, "-"),
   (Mult, "*"),
   (Div, "/"),
   (Mod, "%"),
   (Exp, "**"),
   (And, "&&"),
   (Or, "||"),
   (Xor, "^^"),
   (EQ, "=="),
   (LT, "<"),
   (GT, ">"),
   (LTE, "<="),
   (GTE, ">="),
   (NEQ, "!="),
   (Band, "&"),
   (Bor, "|"),
   (Bxor, "^"),
   (Cons, ":"),
   (Concat, "++")
   ]

-- |The list of prefix operators.
--  All prefix operators have the same precedence and are right-associative.
prefixTable :: [(UOP, String)]
prefixTable = [
   (Neg, "-"),
   (Not, "!"),
   (Bnot, "~")
   ]

-- |The list of postfix operators.
--  All postfix operators have the same precedence and are left-associative.
postfixTable :: [(UOP, String)]
postfixTable = [
   (Fac, "!")
   ]