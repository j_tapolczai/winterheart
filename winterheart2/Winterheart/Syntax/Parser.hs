-- |The parser for Winterheart's language.
-- |Take the input and returns an abstract syntax tree.
-- |Every node in it will have its position in the
-- |auxiliary info, but no type information will
-- |be present (undefined).
module Winterheart.Syntax.Parser where

import Text.ParserCombinators.Parsec hiding (alphaNum)
import Text.ParserCombinators.Parsec.Error
--Just to avoid the full paths in GHCi
import Text.Parsec.Prim hiding (try)

import Data.Functor.Identity
import Control.Applicative hiding (many, optional, (<|>))

import Winterheart.AST
import Winterheart.Syntax.Lexer

type SParser s returnVal = GenParser Char s returnVal

-- |Creates an (Line::Int,Column::Int)-Tuple from a Parsec SourcePos.
pos :: SourcePos -> Position
pos sp = (sourceLine sp, sourceColumn sp) 

-- |Helper function: creates an AST with a position as auxiliary information.
mkAST :: SourcePos -> AST Position -> ASTNode Position
mkAST sp = ASTNode (pos sp)

{------------------------------------------------------------------------------
 LITERALS
------------------------------------------------------------------------------}

pBottom = getPosition >>= (\p -> bottomLit >> return (mkAST p ASTBottom))

pTop = getPosition >>= (\p -> topLit >> return (mkAST p ASTTop))

pBool = do p <- getPosition
           b <- trueLit <|> falseLit
           return $ mkAST p (ASTBool $ if b == "true" then True else False)

pInteger = do p <- getPosition
              i <- integerLit
              return $ mkAST p (ASTInteger i)

pReal = do p <- getPosition
           r <- realLit
           return $ mkAST p (ASTReal r)

pChar = do p <- getPosition
           c <- charLit
           return $ mkAST p (ASTChar c)

pString = do p <- getPosition
             s <- stringLit
             let s' = map (\c -> mkAST p (ASTChar c)) s
             return $ mkAST p (ASTList s')

pList = undefined

pTuple = undefined

pArrayAccess = undefined

{------------------------------------------------------------------------------
 FUNCTION APPLICATION
------------------------------------------------------------------------------}

pFuncApp = undefined

{------------------------------------------------------------------------------
 EXPRESSIONS
------------------------------------------------------------------------------}

-- pOpList and pExpr1 are split for 2 reasons:
-- 1) to avoid creating an LL parser and
-- 2) to have pOpList create a flat list whose elements can later be grouped
--    according to operator precedence, without bothering the parser.

-- |A list of expressions, separated by infix operators.
pOpList =
   do l <- pExpr1
      rest <- many1 $ (:) <$> (Right <$> infixOp) <*> ((:[]) . Left <$> pExpr1)
      return $ groupByPrecedence $ (Left l):(concat rest)

-- |An expression which does not directly contain infix operators.
pExpr1 = pBottom
         <|> pPrefixOp
         <|> pTop
         <|> pBool
         <|> pInteger
         <|> pReal
         <|> pChar
         <|> pString
         <|> pList
         <|> pArrayAccess
         <|> pTuple
         <|> (parens pExpr)
         <|> pFuncApp
         <|> pLambda
         <?> "expression (literal, array access, function application, or lambda)"

--todo: postfix op ?

pExpr = (try pOpList) <|> pExpr1

pPrefixOp = undefined


groupByPrecedence = undefined

{------------------------------------------------------------------------------
 FUNCTION DEFINITION
------------------------------------------------------------------------------}

pLambda = undefined

pProgram = undefined

{------------------------------------------------------------------------------
 PUBLIC FUNCTIONS
------------------------------------------------------------------------------}

-- |Runs a parser on an input.
runParser parser input = parse parser "" input

-- |Parses a complete Winterheart program and returns Either the
-- |AST or a ParserError.
parseInput input filename = parse pProgram filename input