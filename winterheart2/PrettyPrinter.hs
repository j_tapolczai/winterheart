{- |A pretty-printer that prints values in a human-pleasing format.
    Unlike Show, there is no obligation to make the result of pretty-printing
    interpretable by Read or any other class.
-}
module PrettyPrinter (PrettyPrintable (pshow)) where

import Data.List

-- |Pretty-printable types. Minimal and complete definition: pshow.
class PrettyPrintable a where
   -- |Pretty-prints an element.
   pshow :: a -> String

instance PrettyPrintable a => PrettyPrintable [a] where
   pshow x = (concat $ intersperse ", " $ map pshow x)
