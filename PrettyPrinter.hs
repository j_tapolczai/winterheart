{- |A module for the pretty printing of types. Unlike Show, the class PrettyPrintable
    imposes no requirement that the shown type be interpretable by Read.
-}
module PrettyPrinter (PrettyPrintable (..)) where

import Data.List
import Data.Maybe

import ExprTree
import Evaluator

-------------------------------------------------------------------------------
-- CLASSES
-------------------------------------------------------------------------------

-- |Defines the class of pretty-printable types.
class PrettyPrintable pr where
   -- |Similar in purpose to show, but with arbitrary formating allowed
   pPrint :: pr -> String
   
-------------------------------------------------------------------------------
--  INSTANCES
-------------------------------------------------------------------------------

instance PrettyPrintable Pred where
   pPrint p = fromJust $ revSymbolLookup p predNames
   
instance PrettyPrintable SPred where
   pPrint p = fromJust $ revSymbolLookup p spredNames
   
instance PrettyPrintable Op where
   pPrint p = fromJust $ revSymbolLookup p opNames
   
instance PrettyPrintable SOp where
   pPrint p = fromJust $ revSymbolLookup p sopNames

instance PrettyPrintable Expr where
   pPrint (EBool b) = show b
   pPrint (EInt i) = show i
   pPrint (EDouble d) = show d
   pPrint (EString s) = show s

   pPrint (EVar v) = "<" ++ v ++ ">"

   pPrint (EBCast e) = "(bool)" ++ (pPrint e)
   pPrint (EICast e) = "(int)" ++ (pPrint e)
   pPrint (EFCast e) = "(double)" ++ (pPrint e)
   pPrint (ESCast e) = "(string)" ++ (pPrint e)

   pPrint (EOp l op r) = "(" ++ (pPrint l) ++ " " ++ (pPrint op) ++ " " ++ (pPrint r) ++ ")"
   pPrint (ESOp op l) = (pPrint op) ++ "(" ++ (pPrint l) ++ ")"

   pPrint (EPred pred l r) = "(" ++ (pPrint l) ++ ") " ++ (pPrint pred) ++ " (" ++ (pPrint r) ++ ")"
   pPrint (ESPred pred l) = (pPrint pred) ++ "(" ++ (pPrint l) ++ ")"

   pPrint (EFunc f args) = f ++ "(" ++ (concat $ intersperse ", " $ map pPrint args) ++ ")"
