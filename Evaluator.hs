{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveDataTypeable #-}

{- |This module utilizes the parser and the reducer and provides the actual evaluation function for the main program.
    The specification of the language is stored here, as well as the transformation into expression trees. -}
module Evaluator (evaluate,
                  ParseException (ParseException),
                  CastException (CastException),
                  FuncNotFoundException (FNFException),
                  InvalidCastException (InvCastException),
                  VariableNotFoundException (VNFException),
                  CommandNotFoundException (CNFException),
                  opNames, sopNames, predNames, spredNames,
                  castError, revSymbolLookup, cmdNames) where

import Data.Maybe
import Data.Char
import Data.List
import qualified Data.List.Split as LS
import Control.Exception hiding (evaluate)
import Data.Typeable

import Templates
import Parser
import Reducer
import ExprTree
import AutoCasting
import Casting
import LibraryManagement

-------------------------------------------------------------------------------
-- EXCEPTIONS
-------------------------------------------------------------------------------

{- |An exception indictating that the parsing failed.
-}
data ParseException = ParseException deriving (Show, Typeable)

instance Exception ParseException

{- |An exception indictating that the entered command was not found
-}
data CommandNotFoundException = CNFException String deriving (Show, Typeable)

instance Exception CommandNotFoundException

-------------------------------------------------------------------------------
-- LANGUAGE
-------------------------------------------------------------------------------

type SymbolString = String
type Description = String
{- |A library of symbols, consisting of 3-tuples.
    The first element is the actual symbol, the 2nd its string representation
    and the 3rd an optional short description in case the string representation is
    thought to be unfamiliar to the user.
-}
type SymbolLibrary a = [(a, SymbolString, Description)]

-- |Association between binary operators and their string representations.
opNames :: SymbolLibrary Op
opNames = [(Plus, "+", ""),(Minus, "-", ""),(Mult, "*", ""),(Div, "/", ""),
           (Pow, "^", ""), (Mod, "%", "modulo"), (Rem, "\\", "remainder"),
           (Or, "||", "or"),(And, "&&", "and"),(Xor, "~", "xor"),
           (Equiv, "<->", "binary equivalence"),(Concat, "++", "string concatenation")]

-- |The symbol used for variable and function definitions.
assignmentSymbol :: String
assignmentSymbol = ":="
           
{- |Operator precedences, including those for predicates, from the weakest
    binding group to the strongest.
    Unary operators have the highest precedence and are thus not explicitly mentioned
-}
opPrecedences :: [[String]]
opPrecedences = [
                 --weakest: string concatenation
                 ["++"],
                 --binary OR
                 ["||"],
                 --binary AND
                 ["&&"],
                 --XOR, EQUIV, comparison
                 ["~","<->","==","<","<=",">",">=","/="],
                 --MOD, remainder
                 ["%","\\"],
                 --1. order arithmetic operations
                 ["+","-"],
                 --2. order arithmetic operations
                 ["*"],
                 ["/"],
                 --3. order arithmetic operations
                 ["^"]
                 ]
                 
-- |Association between unary operators and their string representations.
sopNames :: SymbolLibrary SOp
sopNames = [(Neg, "-", "minus"),(Not, "!", "binary not")]

-- |Association between binary predicates and their string representations.
predNames :: SymbolLibrary Pred
predNames = [(LEQ, "<=",""),(LE, "<",""),(EEQ, "==",""),(NEQ, "/=", "not equal"),
             (GE, ">",""),(GEQ, ">=","")]
             
-- |Association between unary predicates and their string representations.
spredNames :: SymbolLibrary SPred
spredNames = [(Positive, "pos",""),(Negative, "neg",""),(Zero, "zero",""),
              (IsTrue, "isTrue",""),(IsFalse, "isFalse",""),(IsBool, "isBool",""),
              (IsInt, "isInt",""),(IsDouble, "isDouble",""),(IsString, "isString","")]
              
-- |Association between commands and their string representations.
cmdNames :: SymbolLibrary CmdKind
cmdNames = [(Print,"print",""), (Print,"p",""),
            (Unset,"unset",""),
            (Exit, "exit", ""), (Exit,"e",""),
            (ListVars, "vars", ""),
            (Help, "help", ""),(Help,"h","")]
              
{- |Looks up a symbol in a given library.
    Throws an exception if it is not found.
-}
symbolLookup :: String -> SymbolLibrary b -> Maybe b
symbolLookup name symTable = if null matches then Nothing else Just $ head matches
   where
   matches = map $(sel 1 3) $ filter ((name==) . $(sel 2 3)) symTable
   
{- |The reverse of symbolLookup: retursn the string corresponding to a symbol. -}
revSymbolLookup :: Eq b => b -> SymbolLibrary b -> Maybe String
revSymbolLookup sym symTable = if null matches then Nothing else Just $ head matches
   where
   matches = map $(sel 2 3) $ filter ((sym==) . $(sel 1 3)) symTable

-------------------------------------------------------------------------------
-- LANGUAGE PARSING
-------------------------------------------------------------------------------

{- |Main evaluation function. Takes a string, parses it, builds an expression tree,
    and reduces it to an atomic return vaue, which is then returned.
    
    If the parsing is unsuccessful, an exception is thrown.
-}
evaluate :: String -> Libraries -> (Maybe Expr,Maybe Stmt,Maybe Cmd)
evaluate input libs = 
   let
      results = parse tProgram input
      validResults = filter (null . snd) results
   in
   if null validResults then throw ParseException
   else case (fst . head) validResults of
             (Just expr,_,_) -> (Just $ freduce expr libs, Nothing, Nothing)
             (_,Just stmt,_) -> (Nothing, Just stmt, Nothing)
             (_,_,Just cmd)  -> (Nothing, Nothing, Just cmd)





--bool ::= "True" | "False"
tBool :: Parser String
tBool = (keyWI "true") # (keyWI "false")
          
--digit ::= 0-9
--integer ::= {digit}+                 
tInteger :: Parser String
tInteger = many1 (sat isDigit)

--Double ::= ({digit}+ '.' {digit}+) | ('.' {digit}+)
tDouble :: Parser [String]
tDouble = do {intPart <- tInteger;
             sLift $ item '.';
             frac <- tInteger;
             return [intPart,frac]
         }
         ##
         do {sLift (item '.');
             frac <- tInteger;
             return ["",frac]
         }

--strings         
tString :: Parser String
tString = do item '\"'
             contents <- tString1
             item '\"'
             return contents
    
tString1 :: Parser String
tString1 = do contents <- many (sat (\x -> (not $ specialChar x)))
              contents1 <- optional (do {sLift $ item '\\';
                                         escChar <- sat (\x -> True);
                                         more <- tString1;
                                         return (escChar:more)})
              return $ contents ++ contents1
   where specialChar c = '\\' == c || '\"' == c

--identifier, starting with a letter and consisting of letter, digits and underscore (_)   
tIdentifier :: Parser String
tIdentifier = do firstChar <- sat isAlpha
                 rest <- many (sat $ (flip elem) legalChars)
                 return $ firstChar:rest
   where legalChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_"

-- a list of expressions, interspersed with different kinds of operators/predicates
tExprList :: Parser [Either Expr String]
tExprList = do left <- tExpr1
               op <- tOpOrPred
               right <- tExpr1
               next <- many (do {op <- tOpOrPred; expr <- tExpr1; return [Right op, Left expr]})
               return $ (Left left):(Right op):(Left right):(concat next)
         
--binary op
--op ::= ( "+" | ... )
tOp :: Parser String
tOp = oneOf $ map (token . keyW . $(sel 2 3)) opNames

--unary op
--sop ::= ( "-" | "!" )
tSOp :: Parser String
tSOp = oneOf $ map (token . keyW . $(sel 2 3)) sopNames

--binary pred
--pred ::= ( "<" | ... )
tPred :: Parser String
tPred = oneOf $ map (token . keyW . $(sel 2 3)) predNames

--a binary operator or a binary predicate
tOpOrPred :: Parser String
tOpOrPred = oneOf $ concat [f opNames, f predNames]
   where f = map (token . keyW . $(sel 2 3))

--unary pred
--spred ::= ( "pos" | ... )
tSPred :: Parser String
tSPred = oneOf $ map (token . keyW . $(sel 2 3)) spredNames

--function calls         
tFunc :: Parser (String,[Expr])
tFunc = do fname <- token $ tIdentifier
           token $ item '('
           args <- tArgs
           token $ item ')';
           return (fname, args)

tArgs :: Parser [Expr]
tArgs = do thisArg <- (tExpr # tExpr1);
           otherArgs <- optional (do {token $ item ','; tArgs});
           return (thisArg:otherArgs);
           
--variables
tVar :: Parser String
tVar = do varname <- token $ tIdentifier
          return varname

--tExpr ::= exprList
tExpr :: Parser Expr
tExpr = --list of expressions, separated by operators
        do { lst <- tExprList;
             return $ iExprList opPrecedences lst; }

--tExpr1 ::= bool | integer | Double | '(' tExpr ')' | '(' tExpr1 ')' | (sop tExpr1) | spred '(' (tExpr | tExpr1) ')' | <function name>'(' args ')'
tExpr1 :: Parser Expr
tExpr1 = --atomic value: bool
         do {b <- token tBool; return $ iBool b}
         #
         -- atomic value: Double
         do {(i:f:_) <- token tDouble; return $ iDouble i f}
         #
         -- atomic value: integer
         do {i<- token tInteger; return $ iInt i}
         #
         -- atomic value: string
         do {s <- token tString; return $ iString s}
         #
         -- parenthesized expression
         do {token $ item '('; i <- tExpr; token $ item ')'; return i}
         #
         do {token $ item '('; i <- tExpr1; token $ item ')'; return i}
         #
         -- expression starting with unary operator
         do {sop <- tSOp;
             left <- tExpr1;
             return $ iSOp sop left;
         }
         --expression starting with unary predicate
         #
         do {spred <- tSPred;
             token $ item '(';
             left <- (tExpr # tExpr1);
             token $ item ')';
             return $ iSPred spred left;
         }
        --function call
        #
        do {(f,args) <- tFunc;
            return $ iFunc f args;
        }
        --variable
        #
        do {varname <- tVar;
            return $ iVar varname;
        }
 
--Statements: variable or function definitions 
--stmt ::= tVarAss | tFuncAss
tStmt :: Parser Stmt
tStmt = tVarAss # tFuncAss

--Variable assignment
--varAss ::= varname ":=" (tExpr | tExpr1)
tVarAss :: Parser Stmt
tVarAss = do varname <- token $ many1 (sat isAlpha)
             token $ keyW assignmentSymbol
             varexp <- (tExpr # tExpr1)
             return $ iVarAss varname varexp
             
--Function declaration
--funcass ::= tFuncHead ':=' tExpr | tExpr1
--tFuncHead ::= identifier '(' {identifier}':'('bool'|'int'|'double'|'string')}+')'
tFuncAss :: Parser Stmt
tFuncAss = do head <- tFuncHead
              body <- tExpr # tExpr1
              return $ iFuncAss head body

tFuncHead :: Parser (String, [(String, DataType)])
tFuncHead = do fname <- token $ tIdentifier
               token $ item '('
               params <- tParams
               token $ item ')'
               token $ keyW assignmentSymbol
               return (fname, params)
               
tParams = do thisParam <- token tParam
             otherParams <- optional (do {token $ item ','; tParams})
             return (thisParam:otherParams)
               
tParam :: Parser (String, DataType)
tParam = do vname <- token $ tIdentifier
            token $ item ':'
            dt <- token $ tDataType
            return $ (vname, dt)
            
tDataType :: Parser DataType
tDataType = do dt <- (keyW "bool") # (keyW "int") # (keyW "double") # (keyW "string")
               return $ iDataType dt
               

--Command.
--cmd ::= ':' {a-aA-Z}+ {{^' '}*}*
tCmd :: Parser Cmd
tCmd = do item ':'
          cmdname <- token $ many1 (sat isAlpha)
          args <- many $ token $ many1 (sat (not . isSpace))
          return $ iCmd cmdname args

--program ::= tExpr | tExpr1 | tStmt
tProgram :: Parser (Maybe Expr,Maybe Stmt,Maybe Cmd)
tProgram = do space
              (build (tExpr1 # tExpr) (\x -> (Just x,Nothing,Nothing)))
              #
              (build tStmt (\x -> (Nothing,Just x,Nothing)))
              #
              (build tCmd (\x -> (Nothing,Nothing,Just x)))

-------------------------------------------------------------------------------
-- INTERPRETATION FUNCTIONS
-------------------------------------------------------------------------------

--atomic
iDigit :: Char -> Integer
iDigit = fromIntegral . ((flip (-)) (ord '0')) . ord

iInt :: String -> Expr
iInt = (EInt) . sum . (zipWith (*) $ iterate (*10) 1) . (map iDigit) . reverse

iDouble :: String -> String -> Expr
iDouble int frac = ((EDouble) . (mult*)) (fromIntegral num)
   where mult = (10^^) ((-1) * (length frac))
         (EInt num) = iInt $ int ++ frac

iBool :: String -> Expr
iBool s = if s == "true" then (EBool True)
          else if s == "false" then (EBool False)
          else error ("PARSER[iBool]: Tried to interpret \"" ++ s ++ "\" as Bool!")
   
iString :: String -> Expr
iString = EString

--unary op
iSOp :: String -> Expr -> Expr
iSOp sop x = (ESOp (fromJust $ symbolLookup sop sopNames) x)

--unary pred
iSPred :: String -> Expr -> Expr
iSPred spred x = (ESPred (fromJust $ symbolLookup spred spredNames) x)

--binary op or binary pred
iOpOrPred :: String -> (Expr,Expr) -> Expr
iOpOrPred sym (l,r) = if isJust opMatch then (EOp l opMatch' r)
                      else if isJust predMatch then (EPred predMatch' l r)
                      else error $ "SHOULDN'T HAPPEN: symbol " ++ (show sym) ++ " not found!"
   where opMatch = symbolLookup sym opNames
         opMatch' = fromJust opMatch
         predMatch = symbolLookup sym predNames
         predMatch' = fromJust predMatch


--function calls
iFunc :: String -> [Expr] -> Expr
iFunc = EFunc 

--variables
iVar :: String -> Expr
iVar = EVar

--opListSplit
--opLib's ops go from weakest binding to strongest and are left-associative
iExprList :: [[String]] -> [Either Expr String] -> Expr
iExprList [] [Left xl] = xl
iExprList opLib@(op:ops) input = leftAssoc $ map recCall splitList
   where --calls iExprList recusively, partitioning
         --the expression list by the next-stronger-binding
         --set of operators & predicates
         recCall l@(Left x:[]) = Left x
         recCall l@(Right x:[]) = Right x
         recCall l@(x:xs) = Left $ iExprList ops l
         
         --creates a left-associateive expression tree out of a list
         --of expressions and operator/predicate symbols
         leftAssoc ((Left xl):[]) = xl
         leftAssoc (Left xl:Right s:Left xr:[]) = (iOpOrPred s (xl,xr))
         leftAssoc (Left xl:Right s:Left xr:xs) = leftAssoc $ (Left (iOpOrPred s (xl,xr))):xs
         
         --splits the list by the current operator/predicate symbols
         splitList = LS.split (LS.oneOf (map Right op)) input

iDataType :: String -> DataType
iDataType "bool" = DTBool
iDataType "int" = DTInt
iDataType "double" = DTDouble
iDataType "string" = DTString
iDataType x = error $ "PARSER[iDataType]: Tried to interpret \"" ++ x ++ "\" as data type!"
         
-------------------------------------------------------------------------------
-- INTERPRETATION FUNCTIONS - Statements
-------------------------------------------------------------------------------      
--variable assignments
iVarAss :: String -> Expr -> Stmt
iVarAss = VarAss

--function declarations
iFuncAss :: (String, [(String, DataType)]) -> Expr -> Stmt
iFuncAss (fname, params) body = FuncAss fname body' sig
   where
      boundBody = replace bindParams body
      bindParams (EVar x) = if elem x (map fst params) then (True,(EBound x)) else (False,(EVar x))
      bindParams x = (False,x)
      
      body' :: [Expr] -> Expr
      body' = \args -> replace' args (map fst params) boundBody
      
      sig = map snd params

replace' :: [Expr] -> [String] -> Expr -> Expr
replace' (x:xs) (y:ys) tree = replace' xs ys (replace (rf x y) tree)
   where rf newV bindingName e@(EBound v) = if v==bindingName then (True, newV)
                                            else (False, e)
         rf _ _ e = (False, e)
replace' [] _ tree = tree
-------------------------------------------------------------------------------
-- INTERPRETATION FUNCTIONS - Commands
-------------------------------------------------------------------------------

iCmd :: String -> [String] -> Cmd
iCmd cmdName args = case symbolLookup cmdName cmdNames of
                    Nothing -> throw (CNFException cmdName)
                    (Just cmd) -> Cmd cmd args