{-# LANGUAGE TemplateHaskell #-}

{- |Various little template functions. -}
module Templates where

import Language.Haskell.TH

-- |Selects the ith element of an n-tuple
sel :: Int -- ^ i (1-based)
    -> Int -- ^ n
    -> ExpQ
sel i n = [| \x -> $(caseE [| x |] [alt]) |]
    where alt :: MatchQ
          alt = match pat (normalB rhs) []
 
          pat :: PatQ
          pat = tupP (map varP ass)
 
          rhs :: ExpQ
          rhs = varE(ass !! (i -1)) -- !! is 0 based
 
          ass :: [Name]
          ass = [mkName $ "a" ++ show i | i <- [1..n] ]
