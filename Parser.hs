{- |An monadic parser that provides various primitives to be used for
    language specification.
-}
module Parser (Parser, parse, parse', (#), (##), many, many1, optional,
               sLift, (->>), item, itemI, sat, keyW, keyWI, space, space1,
               token, sepBy, sepBy1, build, oneOf) where

import Control.Monad
import Data.Char

-- |The type of the parser, consisting of a parsing function that takes a string as input and returns the matched part and the remainder of the input string.       
newtype Parser a = Parser (String -> [(a,String)])

-- |Helper function that executes a parser.
parse :: Parser a -> String -> [(a,String)]
parse (Parser f) = f

-- |Same as parse, but with the arguments flipped.
parse' :: String -> Parser a -> [(a,String)]
parse' = flip parse

instance Monad Parser where
   -- |Return a parser that doesn't read anything and returns a.
   return a = Parser (\input -> [(a,input)])
   -- |Extend a parser via concatenation. Executes sndF with every result of parser
   (>>=) parser sndF = Parser (\input -> concat [parse (sndF output) rest |
                                                (output,rest) <- parse parser input])

   
instance MonadPlus Parser where
   -- |The parser that always fails.
   mzero = Parser (\_ -> [])
   -- |Executing two parsers simultaneously.
   p1 `mplus` p2 = Parser (\xs -> parse p1 xs ++ parse p2 xs)

-- |Nondeterminisic choise: executing two parsers simultaneously (synonym for mplus).
(#) :: Parser a -> Parser a -> Parser a
(#) = mplus

-- |Deterministic choice: as mplus, but only chooses one result (the first which is available)
(##) :: Parser a -> Parser a -> Parser a
p1 ## p2 = Parser (\xs -> case parse (p1 `mplus` p2) xs of
                          [] -> []
                          (x:_) -> [x])
                          
-- |0 to * occurrences
many :: Parser a -> Parser [a]
many p = many1 p ## return []

-- |1 to * occurrences
many1 :: Parser a -> Parser [a]
many1 p = do first <- p
             rest <- many p
             return (first:rest)

{- |optionally executes a lifted parser
    If the parser is successful, its result is returned.
    If not, [] is returned, i.e. nothing is read from the input
-}
optional :: Parser [a] -> Parser [a]
optional p = p ## (return [])

{- |Lifts a parser to a composable one, e.g. turns a character parser into a string parser. -}     
sLift :: Parser a -> Parser [a]
sLift p = do res <- p
             return [res]

-- |Executes two lifted parsers in sequence. This superfluous and can be replaced by the more dynamic (>>=).
(->>) :: Parser [a] -> Parser [a] -> Parser [a]
p1 ->> p2 = do res1 <- p1
               res2 <- p2
               return $ concat [res1,res2]
               
-- |Parse single character.
item :: Char -> Parser Char
item c = Parser (\(xs) -> let x = head xs in
                          if (null xs) || (x /= c) then []
                          else [(head xs,tail xs)])
                          
-- |Parse single character; case insensitive.
itemI :: Char -> Parser Char
itemI c = Parser (\(xs) -> let x = toLower $ head xs in
                           if (null xs) || (x /= (toLower c)) then []
                           else [(x,tail xs)])

-- |Parse a character that satisfies a given condition.
sat :: (Char -> Bool) -> Parser Char
sat isOk = Parser (\(xs) -> let x = head xs in
                            if (null xs) || (not $ isOk x) then []
                            else [(head xs,tail xs)])

-- |Reads a string verbatim.
keyW :: String -> Parser String
keyW [] = return []
keyW l@(x:xs) = do item x
                   keyW xs
                   return (x:xs);

-- |Case-insensitively reads a string and returns its lowercase version.
keyWI :: String -> Parser String
keyWI [] = return []
keyWI l@(x:xs) = do itemI x
                    keyWI xs
                    return (x:xs);

-- |Parse 0 to * whitespace characters (as defined by isSpace)                    
space :: Parser String
space = many (sat isSpace)

-- |Parse 1 to * whitespace characters (as defined by isSpace)
space1 :: Parser String
space1 = many1 (sat isSpace)

-- |Reads a token: a parser, followed by 0 to * whitespace characters
token :: Parser a -> Parser a
token p = do res <- p
             space
             return res

{- |Executes 0 to * instances of a parsers p, separated by a parser sep
   BUG: sepBy and sepBy1 can't handle p being a choice between two parsers!
-}
sepBy :: Parser a -> Parser b -> Parser [a]
sepBy p sep = (sepBy1 p sep) ## return []

{- |As sepBy, but 1 to * instances are demanded. Suffers from the same bug as sepBy. -}
sepBy1 :: Parser a -> Parser b -> Parser [a]
sepBy1 p sep = do res <- p
                  rest <- many (do {sep; p})
                  return (res:rest)

{- |@optional@, generalized to an arbitrary number of parsers-}
oneOf :: [Parser a] -> Parser a
oneOf [] = mzero
oneOf (x:[]) = x
oneOf (x:xs) = foldl (\x y -> x # y) x xs

{- |Interpretation function
   executes a parser and then applies a function to the result.
-}
build :: Parser a -> (a -> b) -> Parser b
build (Parser p) f = Parser (\xs -> map (\(x,rest) -> ((f x),rest)) (p xs))