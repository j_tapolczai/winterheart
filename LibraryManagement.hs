{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveDataTypeable #-}

{- |The initial function and predicates libraries as well as the functions
    for modifying these are stores here.
-}
module LibraryManagement (LibraryEntry, Library, Libraries, LibState,
                          addItem, addVar, initialLibs, getLibraries,
                          getItem, getItems, getVar, getLib, funcsName, predsName,
                          remItem, remVar,
                          processStmt) where

import Data.List
import Control.Monad.State

import Templates
import ExprTree
import Casting

{- |A library entry. The first component is the function name
    (not necessarily coincident with a function's symbol),
    the second is the function itself, which takes a list of expressions and returns
    the result, again as an expression. The third component is the function's signature,
    not including its return value, which may be arbitrary.
-}
type LibraryEntry = (String, ([Expr] -> Expr), Signature)

{- |A function library.
-}
type Library = [LibraryEntry]

{- |A collection of libraries.
-}
type Libraries = [(String, Library)]

{- |A State transformer monad corresponding to a collection of function libraries.
-}
type LibState a = StateT Libraries IO a

-- |Adds a new item into a given library. If an item with the same name and signature is already present, it is overwritten.
addItem :: String -- ^The name of the library to which to add an item.
        -> LibraryEntry -- ^The library entry to add.
        -> LibState () -- ^The new library state.
addItem libName newI = modify $ \st -> if isPresent st then replaceEntry st
                                       else addEntry st
   where
      --equality comparison (for name && signature)
      iEq (name1,_,sig1) (name2,_,sig2) = name1 == name2 && sig1 == sig2
      --selects the library named <libName>
      correctLib st = snd $ head $ filter ((libName==) . fst) st
      
      isPresent st = (any (newI `iEq`)) $ correctLib st
      addEntry = ifMap ((libName==) . fst) (\(n,l) -> (n,newI:l))
      replaceEntry = ifMap ((libName==) . fst) (\(n,l) -> (n,replaceEntry' l))
      replaceEntry' = ifMap (iEq newI) (\_ -> newI)
      
      ifMap cond f = map (\x -> if cond x then f x else x)
      
-- |Removes an item (based on name and signature) from a library. If such an item is not present, nothing is done.
remItem :: String -- ^The name of the library from which to remove the item.
        -> LibraryEntry -- ^The library entry to remove.
        -> LibState () -- ^The new library state.
remItem libName item = modify $ \st -> rem st
   where
      iEq (name1,_,sig1) (name2,_,sig2) = name1 == name2 && sig1 == sig2
      
      rem = ifMap ((libName==) . fst) (\(n,l) -> (n,rem' l))
      rem' = filter (not . (iEq item))
      
      ifMap cond f = map (\x -> if cond x then f x else x)
      
-- |Removes the variable with the given name.
remVar :: String -> LibState ()
remVar vName = remItem "vars" (vName, undefined, [])

{- |Adds a new variable. Internally, it is a function with no arguments
    that returns the variable's value.
-}
addVar :: String -> Expr -> LibState ()
addVar vName expr = addItem "vars" (vName,(\_ -> expr),[])


-- |The collection of initial functions ("funcs") and predicates ("preds").
initialLibs :: Libraries
initialLibs = [("funcs",funcs),("preds",preds),("vars",[])]

-- |Returns the list of all libraries.
getLibraries :: Libraries -> [String]
getLibraries = map fst

-- |Gets an item with the given signature and name out of a library.
getItem :: String -- ^The library name.
        -> String -- ^The function's name.
        -> Signature -- ^The signature.
        -> Libraries -- ^The library collection.
        -> Maybe LibraryEntry
getItem libName fName sig libs =
   if null matches then Nothing else Just $ head matches
   where lib = snd $ head $ filter ((libName==) . fst) libs
         matches = filter (\(n,_,s) -> n==fName && sig==s) lib

-- |Gets all items with a given name from a library.
getItems :: String -- ^The library name.
         -> String -- ^The function name.
         -> Libraries -- ^The library collection
         -> [LibraryEntry]
getItems libName fName libs = matches
   where lib = snd $ head $ filter ((libName==) . fst) libs
         matches = filter (\(n,_,_) -> n==fName) lib


-- |Gets an item from among the variables.
getVar :: String -> Libraries -> Maybe Expr
getVar vName libs = case getItem "vars" vName [] libs of
                         Nothing -> Nothing
                         Just (_,x,_) -> Just $ x []

-- |Gets the library with the given name.
getLib :: String -> Libraries -> Library
getLib libName = snd . head . (filter ((libName==) . fst))

{- |Processes a state-altering assignment statement and produces a corresponding LibState ().
    Simply executing in a do-block will alter the LibState accordingly.
-}
processStmt :: Stmt -> LibState ()
processStmt (VarAss name expr) = addVar name expr
processStmt (FuncAss name expr sig) = addItem "funcs" (name,expr,sig)

-- |Union of function and predicate libraries.
funcPreds = concat [funcs,preds]

-------------------------------------------------------------------------------
--  LIBRARIES
-------------------------------------------------------------------------------

-- |The name of the function library.
funcsName :: String
funcsName = "funcs"

{- |The function library.
    Overloading is implemented by creating multiple entires with the same name, with an different signature.
-}
funcs :: Library
funcs = [
         --Control structures
         (,, ) "If" (\((EBool b):e1:e2:_) -> if b then e1 else e2)                      [DTBool, DTAny, DTAny],
         --Op
         (,, ) "Plus" (\((EInt l):(EInt r):_) -> (EInt $ l+r))                          [DTInt, DTInt],
         (,, ) "Minus" (\((EInt l):(EInt r):_) -> (EInt $ l-r))                         [DTInt, DTInt],
         (,, ) "Mult" (\((EInt l):(EInt r):_) -> (EInt $ l*r))                          [DTInt, DTInt],
         (,, ) "Div" (\((EInt l):(EInt r):_) -> if l `rem` r == 0 then EInt $ l `div` r
                                                else EDouble ((i2f l)/(i2f r)))         [DTInt, DTInt],
         (,, ) "Mod" (\((EInt l):(EInt r):_) -> (EInt $ l `mod` r))                     [DTInt, DTInt],
         (,, ) "Rem" (\((EInt l):(EInt r):_) -> (EInt $ l `rem` r))                     [DTInt, DTInt],
         (,, ) "Pow" (\((EInt l):(EInt r):_) -> if r >= 0 then EInt $ l^r
                                                else EDouble $ (i2f l)^^r)              [DTInt, DTInt],
         
         (,, ) "Plus" (\((EDouble l):(EDouble r):_) -> (EDouble $ l+r))                 [DTDouble, DTDouble],
         (,, ) "Minus" (\((EDouble l):(EDouble r):_) -> (EDouble $ l-r))                [DTDouble, DTDouble],
         (,, ) "Mult" (\((EDouble l):(EDouble r):_) -> (EDouble $ l*r))                 [DTDouble, DTDouble],
         (,, ) "Div" (\((EDouble l):(EDouble r):_) -> (EDouble $ l/r))                  [DTDouble, DTDouble],
         (,, ) "Pow" (\((EDouble l):(EDouble r):_) -> EDouble $ l**r)                   [DTDouble, DTDouble],
         
         (,, ) "Or" (\((EBool l):(EBool r):_) -> (EBool (l || r)))                      [DTBool, DTBool],
         (,, ) "And" (\((EBool l):(EBool r):_) -> (EBool (l && r)))                     [DTBool, DTBool],
         (,, ) "Xor" (\((EBool l):(EBool r):_) -> (EBool (not (l==r))))                 [DTBool, DTBool],
         (,, ) "Equiv" (\((EBool l):(EBool r):_) -> (EBool (l==r)))                     [DTBool, DTBool],
         
         --SOp
         (,, ) "Neg" (\((EInt l):_) -> (EInt ((-1)*l)))                                 [DTInt],
         (,, ) "Neg" (\((EDouble l):_) -> (EDouble ((-1)*l)))                           [DTDouble],
         (,, ) "Not" (\((EBool l):_) -> (EBool (not l)))                                [DTBool],
         
         --casts
         (,, ) "ToBool" (\(l:_) -> cast (EBCast l))                                    [DTBool],
         (,, ) "ToBool" (\(l:_) -> cast (EBCast l))                                    [DTInt],
         (,, ) "ToBool" (\(l:_) -> cast (EBCast l))                                    [DTDouble],
         (,, ) "ToBool" (\(l:_) -> cast (EBCast l))                                    [DTString],
         
         (,, ) "ToInt" (\(l:_) -> cast (EICast l))                                     [DTBool],
         (,, ) "ToInt" (\(l:_) -> cast (EICast l))                                     [DTInt],
         (,, ) "ToInt" (\(l:_) -> cast (EICast l))                                     [DTDouble],
         (,, ) "ToInt" (\(l:_) -> cast (EICast l))                                     [DTString],
         
         (,, ) "ToDouble" (\(l:_) -> cast (EFCast l))                                  [DTBool],
         (,, ) "ToDouble" (\(l:_) -> cast (EFCast l))                                  [DTInt],
         (,, ) "ToDouble" (\(l:_) -> cast (EFCast l))                                  [DTDouble],
         (,, ) "ToDouble" (\(l:_) -> cast (EFCast l))                                  [DTString],
         
         (,, ) "ToString" (\(l:_) -> cast (ESCast l))                                  [DTBool],
         (,, ) "ToString" (\(l:_) -> cast (ESCast l))                                  [DTInt],
         (,, ) "ToString" (\(l:_) -> cast (ESCast l))                                  [DTDouble],
         (,, ) "ToString" (\(l:_) -> cast (ESCast l))                                  [DTString],
         
         --other functions
         (,, ) "StrLen" (\((EString s):_) -> (EInt $ genericLength s))                  [DTString],
         (,, ) "StrLeft" (\((EString s):(EInt n):_) ->
                           (EString $ take' n s))                                       [DTString, DTInt],
                           
         (,, ) "StrRight" (\((EString s):(EInt n):_) ->
                           (EString $ drop' ((length' s) - n) s))                       [DTString, DTInt],
                           
         (,, ) "SubStr" (\((EString s):(EInt fro):(EInt len):_) ->
                           (EString $ take' len $ drop' fro s))                         [DTString, DTInt, DTInt],
                           
         (,, ) "Reverse" (\((EString s):_) -> (EString $ reverse s))                    [DTString],
         (,, ) "Concat" (\((EString s1):(EString s2):_) -> (EString $ s1 ++ s2))        [DTString, DTString]
        ]
   where take' n = take (fromIntegral n)
         drop' n = drop (fromIntegral n)
         length' = genericLength

-- |The name of the predicate library.
predsName :: String
predsName = "preds"

-- |Predicate library.
preds :: Library
preds = [
         --Pred
         (,, ) "LEQ" (\((EInt l):(EInt r):_) -> (EBool $ l<=r))                         [DTInt, DTInt],
         (,, ) "LE" (\((EInt l):(EInt r):_) -> (EBool $ l<r))                           [DTInt, DTInt],
         (,, ) "EEQ" (\((EInt l):(EInt r):_) -> (EBool $ l==r))                         [DTInt, DTInt],
         (,, ) "NEQ" (\((EInt l):(EInt r):_) -> (EBool $ l/=r))                         [DTInt, DTInt],
         (,, ) "GE" (\((EInt l):(EInt r):_) -> (EBool $ l>r))                           [DTInt, DTInt],
         (,, ) "GEQ" (\((EInt l):(EInt r):_) -> (EBool $ l>=r))                         [DTInt, DTInt],
         
         (,, ) "LEQ" (\((EDouble l):(EDouble r):_) -> (EBool $ l<=r))                   [DTDouble, DTDouble],
         (,, ) "LE" (\((EDouble l):(EDouble r):_) -> (EBool $ l<r))                     [DTDouble, DTDouble],
         (,, ) "EEQ" (\((EDouble l):(EDouble r):_) -> (EBool $ l==r))                   [DTDouble, DTDouble],
         (,, ) "NEQ" (\((EDouble l):(EDouble r):_) -> (EBool $ l/=r))                   [DTDouble, DTDouble],
         (,, ) "GE" (\((EDouble l):(EDouble r):_) -> (EBool $ l>r))                     [DTDouble, DTDouble],
         (,, ) "GEQ" (\((EDouble l):(EDouble r):_) -> (EBool $ l>=r))                   [DTDouble, DTDouble],
         
         (,, ) "LEQ" (\((EBool l):(EBool r):_) -> (EBool $ l<=r))                       [DTBool, DTBool],
         (,, ) "LE" (\((EBool l):(EBool r):_) -> (EBool $ l<r))                         [DTBool, DTBool],
         (,, ) "EEQ" (\((EBool l):(EBool r):_) -> (EBool $ l==r))                       [DTBool, DTBool],
         (,, ) "NEQ" (\((EBool l):(EBool r):_) -> (EBool $ l/=r))                       [DTBool, DTBool],
         (,, ) "GE" (\((EBool l):(EBool r):_) -> (EBool $ l>r))                         [DTBool, DTBool],
         (,, ) "GEQ" (\((EBool l):(EBool r):_) -> (EBool $ l>=r))                       [DTBool, DTBool],
         
         (,, ) "EEQ" (\((EString l):(EString r):_) -> (EBool $ l==r))                   [DTString, DTString],
         (,, ) "NEQ" (\((EString l):(EString r):_) -> (EBool $ l/=r))                   [DTString, DTString],
         
         --SPred
         (,, ) "Positive" (\((EInt l):_) -> (EBool $ l >= 0))                           [DTInt],
         (,, ) "Positive" (\((EDouble l):_) -> (EBool $ l >= 0))                        [DTDouble],
         (,, ) "Negative" (\((EInt l):_) -> (EBool $ l < 0))                            [DTInt],
         (,, ) "Negative" (\((EDouble l):_) -> (EBool $ l < 0))                         [DTDouble],
         (,, ) "Zero" (\((EInt l):_) -> (EBool $ l == 0))                               [DTInt],
         (,, ) "Zero" (\((EDouble l):_) -> (EBool $ l == 0))                            [DTDouble],
         
         (,, ) "IsTrue" (\((EBool l):_) -> (EBool $ l == True))                         [DTBool],
         (,, ) "IsFalse" (\((EBool l):_) -> (EBool $ l == False))                       [DTBool],
         
         -- Is<Data Type>
         (,, ) "IsBool" (\((EBool l):_) -> (EBool True))                                [DTBool],
         (,, ) "IsBool" (\((EInt l):_) -> (EBool False))                                [DTInt],
         (,, ) "IsBool" (\((EDouble l):_) -> (EBool False))                             [DTDouble],
         (,, ) "IsBool" (\((EString l):_) -> (EBool False))                             [DTString],

         (,, ) "IsInt" (\((EBool l):_) -> (EBool False))                                [DTBool],
         (,, ) "IsInt" (\((EInt l):_) -> (EBool True))                                  [DTInt],
         (,, ) "IsInt" (\((EDouble l):_) -> (EBool False))                              [DTDouble],
         (,, ) "IsInt" (\((EString l):_) -> (EBool False))                              [DTString],
         
         (,, ) "IsDouble" (\((EBool l):_) -> (EBool False))                             [DTBool],
         (,, ) "IsDouble" (\((EInt l):_) -> (EBool False))                              [DTInt],
         (,, ) "IsDouble" (\((EDouble l):_) -> (EBool True))                            [DTDouble],
         (,, ) "IsDouble" (\((EString l):_) -> (EBool False))                           [DTString],
         
         (,, ) "IsString" (\((EBool l):_) -> (EBool False))                             [DTBool],
         (,, ) "IsString" (\((EInt l):_) -> (EBool False))                              [DTInt],
         (,, ) "IsString" (\((EDouble l):_) -> (EBool False))                           [DTDouble],
         (,, ) "IsString" (\((EString l):_) -> (EBool True))                            [DTString]
        ]