{-# LANGUAGE TemplateHaskell #-}

import Prelude hiding (catch)
import Data.Char
import Data.List
import Data.Maybe
import Control.Monad
import Control.Monad.State
import System.IO
import System.Console.ANSI
import Control.Exception.Lifted hiding (evaluate)
import System.Exit

import Templates
import Evaluator
import ExprTree
import LibraryManagement
import PrettyPrinter


-- |A little attribute file.
appInfo :: String -> Maybe String
appInfo key = if (null hits) then Nothing else Just $ head hits
   where hits = map snd $ filter ((key==) . fst) $ lib
         lib = [("appName", "Winterheart"),
                ("appSubTitle", "The premier Haskell calculator"),
                ("appVer", "v7.1")
                ]


-------------------------------------------------------------------------------
-- HELP TEXTS
-------------------------------------------------------------------------------   
             
helpText :: Libraries -> IO ()
helpText libs =
   do introText
   
      putStr "Type "
      putVivid "\":v[ars]\""
      putStrLn " for a list of defined variables."
      putStrLn ""

      putStr "Variables can be set with "
      colorPrinter "Var" Vivid Cyan
      putVivid ":="
      colorPrinter "Expression" Vivid Yellow
      putStrLn ","
      putStr "printed with "
      putVivid ":p[rint] "
      colorPrinter "Var1" Vivid Cyan
      putVivid " ... "
      colorPrinter "VarN" Vivid Cyan
      putStr " and unset with "
      putVivid ":unset "
      colorPrinter "Var1" Vivid Cyan
      putVivid " ... "
      colorPrinter "VarN" Vivid Cyan
      putStrLn "."
      putStrLn ""
      
      putStr "Functions can be defined with "
      putVivid "Name("
      colorPrinter "Arg1" Vivid Cyan
      putVivid ":"
      colorPrinter "Type1" Vivid Green
      putVivid ",...,"
      colorPrinter "ArgN" Vivid Cyan
      putStr ":"
      colorPrinter "TypeN" Vivid Green
      putVivid "):="
      colorPrinterLn "Expression" Vivid Yellow
      putStr "Example: "
      putVivid "add3("
      colorPrinter "x" Vivid Cyan
      putVivid ":"
      colorPrinter "int" Vivid Green
      putVivid ","
      colorPrinter "y" Vivid Cyan
      putVivid ":"
      colorPrinter "int" Vivid Green
      putVivid ","
      colorPrinter "z" Vivid Cyan
      putVivid ":"
      colorPrinter "int" Vivid Green
      putVivid "):=x+y+z"
      putStrLn ""
   
      line
      putVivid "Allowed data types:        "
      putStrLn "bool (True, False), int, double, string"
      putStrLn (take 80 $ iterate id '_')
      putVivid   "Allowed binary operators:  "
      listLib (map appendDesc opNames) 27 80
      line
      putVivid  "Allowed unary operators:   "
      listLib (map appendDesc sopNames) 27 80
      line
      putVivid   "Allowed binary predicates: "
      listLib (map appendDesc predNames) 27 80
      line
      putVivid   "Allowed unary predicates:  "
      listLib (map ((++"()") . $(sel 2 3)) spredNames) 27 80
      line
      putVivid   "Allowed functions:         "
      listLib (nub $ map $(sel 1 3) (getLib funcsName libs)) 27 80
      line
      listVars libs
   where line = putStrLn (take 80 $ iterate id '_')
         appendDesc (_,name,desc) = if null desc then name else name ++ " (" ++ desc ++ ")"

listVars :: Libraries -> IO ()
listVars libs = do putStr "Defined variables:         "
                   listLib (map $(sel 1 3) vars) 27 80
   where vars = (getLib "vars" libs)
         
listVarsS :: Libraries -> IO ()
listVarsS libs = 
   if null vars then liftIO $ putStrLn "No variables set. Set variables with <Variable Name>:=<Value>"
   else do putStr "Defined variables: "
           listLib (map $(sel 1 3) vars) 19 80
   where vars = (getLib "vars" libs)
              
--lists library functions              
listLib :: [String] -> Integer -> Integer -> IO () 
listLib list numSp lineLen = boxPrint bp1 $ PB {toPrint=list,
                                                lineLength=lineLen,
                                                initSpace=numSp,
                                                charsLeft=lineLen-numSp,
                                                firstLine=True}
                                                
--prints an introductory text
introText :: IO ()
introText =
   do putStrLn "Usage: type in expressions and press [Enter] to evaluate them."

      putStr "Type "
      putVivid "\":h[elp]\""
      putStrLn " for a list of supported data types and symbols."
      
      putStr "Type "
      putVivid "\":e[xit]\""
      putStrLn " to quit."
      
titleText :: IO ()
titleText =
   do putVividLn $ "{" ++ fromJust (appInfo "appName") ++ "}"
      putVividLn $ "   " ++ fromJust (appInfo "appSubTitle")
      
      

      
      
-------------------------------------------------------------------------------
-- MAIN
-------------------------------------------------------------------------------   
     
-- |Starts the calculator in the console.     
main :: IO ()
main = do x <- runStateT main' initialLibs
          return ()
   where main' :: LibState ()
         main' =
            do liftIO $ clearScreen
               liftIO $ setCursorPosition 0 0
               liftIO $ setTitle $ fromJust (appInfo "appName") ++ " " ++ fromJust (appInfo "appVer")
               liftIO $ titleText
               liftIO $ introText
               --main loop
               repl

-- |REPL loop               
repl :: LibState ()
repl =   do liftIO $ putStr "> "
            liftIO $ hFlush stdout
            line <- liftIO getLine
            libs <- get
            --ignore empty lines
            if (and $ map isSpace $ line) then
               repl
            -- evaluate input
            else
               let getElem x = snd . head . (filter ((x==) . fst)) in
               --evaluate while catching exceptions
               do catches
                   (case evaluate line libs of
                         --expression: print result
                         (Just expr,_,_) -> liftIO $ resPrint expr
                         --statement: process (add variable/function def.)
                         (_,Just stmt,_) -> processStmt stmt
                         --command: call appropriate command handler
                         (_,_,Just (Cmd cmd args)) -> getElem cmd cmdHandlers args libs)
                   [parseHandler,
                    castHandler,
                    fnfHandler,
                    invCastHandler,
                    cnfHandler,
                    vnfHandler,
                    stackOverflowHandler]
                  --call REPL again
                  repl

-------------------------------------------------------------------------------
-- PRINTING HELPERS
-------------------------------------------------------------------------------

-- |A Printbox specifying a box in the console.
data PrintBox a = PB {toPrint::[a],
                      --required initial value: lineLength - initSpace
                      charsLeft::Integer,
                      lineLength::Integer,
                      initSpace::Integer,
                      firstLine::Bool} deriving (Eq, Show)


{- |Standard print box function that prints a list of strings while staying strictly 
    inside the box limits. 
-}                    
bp1 :: PrintBox String -> IO(PrintBox String)
bp1 (PB (p:ps) left line init firstLine) =
   let
      last = null ps
      toPrint = if last then p else (p ++ ", ")
      len = genericLength toPrint
      newL = if left - len <= 0 then line - init else left - len
   in
      do if left - len <= 0 then do putStrLn ""
                                    return (PB (p:ps) newL line init False)
         else do if left == line - init && not firstLine then
                    putStr $ replicate (fromIntegral init) ' '
                 else return ()
                 putStr $ p ++ (if last then "" else ", ")
                 return (PB ps newL line init firstLine)


-- |Prints a printBox.
boxPrint :: (PrintBox a -> IO(PrintBox a)) -> PrintBox a -> IO()
boxPrint _ (PB [] _ _ _ _) = putStrLn ""
boxPrint bp arg@(PB (p:ps) _ _ _ _) = do res <- bp arg
                                         boxPrint bp res
                                         return ()

-- |hPutStr stderr                                         
putErr :: String -> IO()
putErr = hPutStr stderr

-- |hPutStrLn stderr  
putErrLn :: String -> IO()
putErrLn = hPutStrLn stderr

-- |Prints an error or a list of errors to a console in the form "Error: ..."
printError :: (IO ()) -- ^The error printing method (e.g. putStrLn calls)
           -> Bool -- ^Whether to make a newline at the beginning.
           -> Bool -- ^Whether to make a newline after the end.
           -> IO ()
printError error startNL endNL =
   do setSGR [SetColor Foreground Vivid Red]
      if startNL then hPutStrLn stderr "Error:"
                 else hPutStr stderr "Error: "
      error
      if endNL then hPutStrLn stderr ""
               else return ()
      setSGR [SetColor Foreground Dull White]
      
-- |Results printer: prints an atomic expression in color
resPrint (EBool b) = colorPrinterLn (show b) Vivid Yellow
resPrint (EInt i) = colorPrinterLn (show i) Vivid Green
resPrint (EDouble f) = colorPrinterLn (show f) Vivid Cyan
resPrint (EString s) = colorPrinterLn (show s) Vivid Magenta

-- |Prints a string in color.
colorPrinterLn s intens color = do setSGR [SetColor Foreground intens color]
                                   putStrLn s
                                   setSGR [SetColor Foreground Dull White]

-- |Prints a string in color.                                   
colorPrinter s intens color = do setSGR [SetColor Foreground intens color]
                                 putStr s
                                 setSGR [SetColor Foreground Dull White]

-- |Prints a string in vivid white.                                 
putVivid s = colorPrinter s Vivid White
-- |Prints a string in vivid white.
putVividLn s = colorPrinterLn s Vivid White
                                 

-------------------------------------------------------------------------------
-- COMMAND HANDLERS
-------------------------------------------------------------------------------

cmdHandlers :: [(CmdKind, [String] -> Libraries -> (StateT Libraries IO) ())]
cmdHandlers = [(Print, varPrinter),
               (Unset, varUnsetter),
               (Exit, \_ _ -> liftIO $ exitSuccess),
               (ListVars, \_ libs -> liftIO $ listVarsS libs),
               (Help, \_ libs -> liftIO $ helpText libs)]
               
   where varPrinter (x:xs) libs = case (getVar x libs) of
                                       Nothing -> do return ()
                                                     varPrinter xs libs
                                       Just v -> do liftIO $ putStr $ x ++ ": " 
                                                    liftIO $ putStrLn $ pPrint v
                                                    varPrinter xs libs
         varPrinter [] libs = return ()
         
         varUnsetter (x:xs) libs = do remVar x
                                      varUnsetter xs libs
         varUnsetter [] libs = return ()
         
-------------------------------------------------------------------------------
-- EXCEPTION HANDLERS
-------------------------------------------------------------------------------

parseHandler :: Handler (StateT Libraries IO) ()
parseHandler = Handler (\ParseException ->
   do liftIO $ printError (hPutStrLn stderr "Could not parse expression.") False False)
   
cnfHandler :: Handler (StateT Libraries IO) ()
cnfHandler = Handler (\(CNFException cmdname) ->
   do liftIO $ printError (hPutStrLn stderr $ "Command \"" ++ cmdname ++ "\" not found.") False False)

castHandler :: Handler (StateT Libraries IO) ()
castHandler = Handler (\(CastException errPos sig) ->
   do liftIO $ printError (castError errPos sig) True False)

fnfHandler :: Handler (StateT Libraries IO) ()
fnfHandler = Handler (\(FNFException fname fsig similarsigs) ->
   let
      showSig = (concat . intersperse ", " . map (show))
      showFunc sig = fname ++ "(" ++ (showSig sig) ++ ")"
      sigList list = (foldl (>>) (return ())) $ map putErrLn list
                                   
      error = do if null fsig then putErrLn $ "Function " ++ fname ++ " not found."
                 else putErrLn $ "Function " ++ (showFunc fsig) ++ " not found."
                 if null similarsigs then return ()
                 else do putErrLn "Did you mean?"
                         sigList $ map showFunc similarsigs
   in
      do liftIO $ printError error False False)

invCastHandler :: Handler (StateT Libraries IO) ()      
invCastHandler = Handler (\(InvCastException expr dt) ->
   do liftIO $ printError (putErrLn $ "Tried to cast \"" ++ (pPrint expr) ++ "\" to " ++ (show dt) ++ ". This cast is invalid.") False False)

vnfHandler :: Handler (StateT Libraries IO) ()
vnfHandler = Handler (\(VNFException vname) ->
   do liftIO $ printError (putErrLn $ "Variable \"" ++ vname ++ "\" is unknown.") False False)

stackOverflowHandler :: Handler (StateT Libraries IO) ()
stackOverflowHandler = Handler (\StackOverflow ->
   do liftIO $ printError (hPutStrLn stderr "Stack overflow. Did you perform a non-terminating computation?") False False)