{-# LANGUAGE DeriveDataTypeable #-}

{- |The (currently inefficient) mumbo-jumbo pertaining to automatically casting
    a given list of atomic expressions and a target function signature is performed here.
    Permissible automatic casts are specified via @castGraph@. Multi-part casts
    are permitted and will be performed in succession.
-}
module AutoCasting (castError, castToSig, sigDiff, sigDiff', castGraph, exprs2DT,
                    Graph(Gr), Vertex (Ve),
                    CastException (CastException)) where

import Data.List
import System.IO
import Data.Maybe
import Control.Monad
import Control.Applicative hiding (many, optional)
import Control.Exception
import Data.Typeable

import ExprTree

-------------------------------------------------------------------------------
-- EXCEPTIONS
-------------------------------------------------------------------------------

{- |An exception indicating a failed automatic cast.
    The first argument are the positions of the arguments (1-based)
    which could not be cast, the second is the signature of the function.
-}
data CastException = CastException [Integer] Signature
     deriving (Show, Typeable)

instance Exception CastException

-------------------------------------------------------------------------------

-- |A vertex in the casting graph.
newtype Vertex a = Ve a deriving (Show, Eq)
-- |The type casting graph, with each vertex storing a list of its successors.
newtype Graph a = Gr [(Vertex a, [Vertex a])] deriving (Show, Eq)
-- |A series of vertices constituting a path in the graph.
newtype Path a = Pa [Vertex a] deriving (Show, Eq)

-- |Unwrapped for @Path@.
fromPath :: Eq a => Path a -> [Vertex a]
fromPath (Pa list) = list

-- |Prints an error message about parameters that couldn't be cast.
castError :: [Integer] -> Signature -> IO()
castError (x:xs) sig = do hPutStrLn stderr $ "Couldn't cast argument " ++ (show x) ++ " to " ++ (show (sig !! (x'-1))) ++ "!"
                          castError xs sig
   where x' = fromIntegral x
castError [] _ = return ()

{- | A generalization dropWhile, inspired by mapAccum:
     takes an accumulator function, an initial value,
     a list and starts dropping elements until the accumulator function
     returns @(_,False)@.
     
     @dropWhile f = snd $ dropAccumL (\a x -> (a, f x)) id@
-}
dropAccumL :: (acc -> x -> (acc,Bool)) -- ^The accumulator function.
           -> acc -- ^The initial value for the accumulator.
           -> [x] -- ^The input list.
           -> (acc,[x]) -- ^The result, consisting of the return value of the accumulator and the remaining list elements.
dropAccumL f cur l@(x:xs) = if snd next then dropAccumL f (fst next) xs else (fst next, l)
   where next = f cur x
dropAccumL _ cur [] = (cur,[])

{- |Returns Just the data type of an atomic expression.
    If the expression is not atomic, DTAny is returned.
-}
expr2DT :: Expr -> DataType
expr2DT (EBool _) = DTBool
expr2DT (EInt _) = DTInt
expr2DT (EDouble _) = DTDouble
expr2DT (EString _) = DTString
expr2DT _ = DTAny

{- |Returns Just the data types of a list of atomic expression.
    For non-atomic expressions, DTAny
-}
exprs2DT :: [Expr] -> [DataType]
exprs2DT = map expr2DT

{- |Represents the relationship of which data types can be automatically cast to which.
    It is NECESSARY that this graph be acyclic.
-}
castGraph :: Graph DataType
castGraph = Gr [(Ve DTAny, []),
                (Ve DTBool, [Ve DTAny, Ve DTString]),
                (Ve DTInt, [Ve DTAny, Ve DTString, Ve DTDouble]),
                (Ve DTDouble, [Ve DTAny, Ve DTString]),
                (Ve DTString, [Ve DTAny])]
             
{- |Outputs the difference between a present and a desired signature
 the distance is the total number of necessary casts
-}
sigDiff :: [DataType] -- ^The actual data types of the expressions.
        -> Signature -- ^The signature of the function which is to be called.
        -> Maybe Integer -- ^Just the sum of the number of necessary casts, if automatic casting is possible. If it is not, Nothing.
sigDiff presentSig targetSig = if (length presentSig) /= (length targetSig) then Nothing
                               else foldl maybeAdd (Just 0) $ map pathLength
                                                            $ (map (uncurry autoCastPath))
                                                            $ zip presentSig targetSig
   where pathLength = fmap (\(Pa l) -> length' l - 1)
         maybeAdd a b = fmap (+) a <*> b
         length' = genericLength

-- |A wrapper for sigDiff.   
sigDiff' :: [Expr] -> Signature -> Maybe Integer
sigDiff' presentSig = sigDiff $ map expr2DT presentSig

{- |Returns Just a series of casts that will convert the source data type into the target data type.
    if the source DT cannot be automatically cast to the target DT, Nothing is returned.
-}
autoCastPath :: DataType -- ^The source data type
             -> DataType -- ^The target data type
             -> Maybe (Path DataType) -- ^The path from GOAL TO START, with the start node ommitted.
autoCastPath startDT goalDT = if (isNothing searchRes) || (head . fromPath . fromJust $ searchRes) == (Ve goalDT) then searchRes else Nothing
   where
   searchRes = fst $ dps [] [] castGraph (Ve startDT) (Ve goalDT)

{- |Performas a dps on the cast graphs
-}   
dps :: Eq a => [Vertex a] -- ^Already visited nodes
    -> [Vertex a] -- ^The path so far
    -> Graph a -- ^The cast graph
    -> Vertex a -- ^The current vertex
    -> Vertex a -- ^The goal
    -> (Maybe (Path a),[Vertex a]) -- ^The resulting path (from goal to start) and the list of visited nodes.
dps visited path gr@(Gr adj) current goal =
   if current `elem` visited then (Nothing, current:visited)
   else if current == goal then (Just (Pa (current:path)), current:visited)
   else if (null successors) then (Nothing, current:visited)
   else (fst) (dropAccumL (execDPS) (Nothing,visited) successors)
   
   where
   successors = filter (flip notElem visited) $ snd $ head $ filter ((current==) . fst) adj
   
   execDPS (_, accumVisited) node =
         let result@(respath,newVisited) = dps (current:accumVisited) (current:path) gr node goal
         in (result, isNothing respath)

-- |Converts a path into a series of casts which are wrapped around an expression.
path2ExprTree :: Expr -- ^The atomic expression to cast.
              -> Path DataType -- ^The series of casts to be performed.
              -> Expr -- ^The initial expression, wrapped in the given casts.
path2ExprTree expr (Pa []) = expr
path2ExprTree expr (Pa ((Ve x):rest@(xs:ys))) = cast (path2ExprTree expr (Pa rest)) x
   where
   cast e DTBool = (EBCast e)
   cast e DTInt = (EICast e)
   cast e DTDouble = (EFCast e)
   cast e DTString = (ESCast e)
   cast e _ = e
path2ExprTree expr (Pa (x:xs)) = expr

{- |Tries to cast a list of expressions to a desired signature.
    If successful, a modified list of expresions, with appropriate Cast-nodes inserted,
    is returned.
    If one or more expression cannot be automatically cast, the list of the positions
    of these failure cases is returned on the Right.
-}
castToSig :: [Expr] -- ^List of expressions to cast.
          -> Signature -- ^The target signature
          -> Either [Expr] [Integer] -- ^Left [the cast expressions] if possible, Right [the positions of the uncastable expressions] if not.
castToSig exprs sig = if (null failures) then Left castExprs else Right failures
   where
      convertToPath (startDT,s) = autoCastPath startDT s
      
      paths = map convertToPath $ zip (map expr2DT exprs) sig
      
      --only one of these will be evaluated
         -- list of positions where arguments couldn't be converted
      failures = map fst $ filter (isNothing . snd) $ zip [1..] paths
         -- exprs, wrapped in casts
      castExprs = map (\(e, path) -> path2ExprTree e (fromJust path)) $ zip exprs paths