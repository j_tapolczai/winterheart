{-# LANGUAGE DeriveDataTypeable #-}

module Casting (cast, i2b, i2f, i2s, b2i, b2s, f2i, f2s, s2b,
                InvalidCastException (InvCastException)) where

import Control.Exception
import Data.Char
import Data.Typeable hiding (cast)

import ExprTree

-------------------------------------------------------------------------------
-- EXCEPTIONS
-------------------------------------------------------------------------------

{- |An exception indicating an invalid manual cast. The first argument is the
    expression on which a cast was attempted and the second is the type
    to which the cast was attempted.
-}
data InvalidCastException = InvCastException Expr DataType deriving (Show, Typeable)

instance Exception InvalidCastException

-------------------------------------------------------------------------------
--  CASTING
-------------------------------------------------------------------------------

-- |Performs casting of atomic values and atomic values only.
cast :: Expr -> Expr
cast (EBCast e@(EBool b)) = e
cast (EBCast (EInt i)) = (EBool (i2b i))
cast (EBCast e@(EDouble f)) = (EBool ((i2b . f2i) f))
cast (EBCast (EString s)) = (EBool (s2b s))

cast (EICast (EBool b)) = (EInt (b2i b))
cast (EICast e@(EInt i)) = e
cast (EICast (EDouble i)) = (EInt (f2i i))
cast (EICast e@(EString s)) = throw (InvCastException e DTInt)

cast (EFCast (EBool b)) = (EDouble ((i2f . b2i) b))
cast (EFCast (EInt i)) = (EDouble (i2f i))
cast (EFCast e@(EDouble _)) = e
cast (EFCast e@(EString s)) = throw (InvCastException e DTDouble)

cast (ESCast (EBool b)) = (EString (b2s b))
cast (ESCast (EInt i)) = (EString (i2s i))
cast (ESCast (EDouble f)) = (EString (f2s f))
cast (ESCast e@(EString s)) = e

-------------------------------------------------------------------------------
--  CASTING HELPERS
-------------------------------------------------------------------------------

i2b :: Integer -> Bool
i2b 0 = False
i2b _ = True

b2i :: Bool -> Integer
b2i True = 1
b2i False = 0

s2b :: String -> Bool
s2b s = if s' == "true" then True
        else if s' == "false" then False
        else throw (InvCastException (EString s) DTBool)
        where s' = (map toLower s)

b2s :: Bool -> String
b2s True = "True"
b2s False = "False"

i2s :: Integer -> String
i2s = show

f2s :: Double -> String
f2s = show

i2f :: Integer -> Double
i2f = fromIntegral

f2i :: Double -> Integer
f2i = truncate