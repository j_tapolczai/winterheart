{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveDataTypeable #-}

{- |This modules specifies the reduction algorithm for the expression tree.
-}
module Reducer (Signature, Library,
                CastReducible (..),
                FuncNotFoundException (FNFException),
                VariableNotFoundException (VNFException), lazyArgs)
where

import Data.List
import Data.Maybe
import Control.Applicative hiding (many, optional)
import Control.Exception
import Data.Typeable hiding (cast)

import Templates
import AutoCasting
import ExprTree
import LibraryManagement
import Casting

-------------------------------------------------------------------------------
-- EXCEPTIONS
-------------------------------------------------------------------------------

{- |An exception indicating that a function with a given name and signature was
    not found.
    The third argument is the list of functions with the same name, but with
    a different signature.
-}
data FuncNotFoundException = FNFException String Signature [Signature] deriving (Show, Typeable)

instance Exception FuncNotFoundException

{- |An exception indicating that a variable with the given name was not found. The argument is the variable name.
-}
data VariableNotFoundException = VNFException String deriving (Show, Typeable)

instance Exception VariableNotFoundException
 
-------------------------------------------------------------------------------
--  CLASSES 
-------------------------------------------------------------------------------

{- |The class of types which can be reduced to atomic values, with optional casts.
    Instances must fulfill the following law: fatomic (freduce a) = True.
-}    
class CastReducible r where
   --true iff a tree can no longer be reduced
   fatomic :: r -> Bool
   -- Reduces expression trees
   freduce :: r -> Libraries -> r
   --Casts the children of inner nodes
   ---to the appropriate instances, depending on op
   fcast ::  r -> r
     
-------------------------------------------------------------------------------
--  INSTANCES
-------------------------------------------------------------------------------

instance CastReducible Expr where
   fatomic (EBool _) = True
   fatomic (EInt _) = True
   fatomic (EDouble _) = True
   fatomic (EString _) = True
   fatomic _ = False
   
   --atomic
   freduce e@(EBool b) _ = e
   freduce e@(EInt i) _ = e
   freduce e@(EDouble i) _ = e
   freduce e@(EString s) _ = e
   
   --variables
   --freduce (EVar name) = freduce $ head $ map snd $ filter ((name==) . fst) vars
   freduce (EVar name) libs = case getVar name libs of
                                   Nothing -> throw (VNFException name)
                                   Just expr -> freduce expr libs
   
   --casts
   freduce (EBCast l) libs = fcast $ EBCast (freduce l libs)
   freduce (EICast l) libs = fcast $ EICast (freduce l libs)
   freduce (EFCast l) libs = fcast $ EFCast (freduce l libs)
   freduce (ESCast l) libs = fcast $ ESCast (freduce l libs)
   
   --binary operators
   freduce (EOp left op right) libs = freduce' (getLib funcsName libs) (show op) [left, right] libs True
   --unary operators
   freduce (ESOp op left) libs = freduce' (getLib funcsName libs) (show op) [left] libs True
   
   --binary predicates
   freduce (EPred pred left right) libs = freduce' (getLib predsName libs) (show pred) [left, right] libs True
   --unary predicates
   freduce (ESPred spred left) libs = freduce' (getLib predsName libs) (show spred) [left] libs True
   
   --functions
   freduce (EFunc fname args) libs = freduce' (getLib funcsName libs) fname args libs False
   
   --atomic casts
   --provided a correct & exhaustive implementation of freduce, only these are required
   fcast = cast

{- |Returns a member of redArgs where the signature indicates a concrete type (Bool, Int,...)
    and a member of args where it indicates DTAny
-}
lazyArgs :: [Expr] -- ^args - the unreduced arguments.
         -> [Expr] -- ^redArgs - the reduced arguments.
         -> Signature -- ^The function signature.
         -> [Expr] -- ^The mish-mash of reduced and unreduced arguments.
lazyArgs args redArgs sig = map lazyAny zipped
   where zipped = zip3 args redArgs (sig ++ (repeat DTAny))
         lazyAny (a,_,DTAny) = a
         lazyAny (_,r,_) = r
         
-- |Reduces a function call, performinc automatic argument casting if possible.
freduce' :: Library -- ^The library in which to look up the function name.
         -> String -- ^The function's name.
         -> [Expr] -- ^The list of arguments.
         -> Libraries -- ^The current libraries.
         -> Bool -- ^Whether the function call is inline. This only affects error cases: if true, a CastException is thrown in the case of invalid arguments; if false, an FNFException is thrown instead.
         -> Expr -- ^The return value of the function.
freduce' library name args libs isInline = 
   let
      --recursively reduce arguments
      redArgs = map ((flip freduce) libs) args
      
      --reduce only those arguments for which a function signature demands a concrete
      --type.
      --for those where any type is allowed (DTAny), return the unreduced argument.
      lazyArgs sig = map lazyAny zipped
         where zipped = zip3 args redArgs (sig ++ (repeat DTAny))
               lazyAny (a,_,DTAny) = a
               lazyAny (_,r,_) = r
      
      --sorts Maybes and puts Nothings last
      sorter Nothing _ = GT
      sorter (Just _) Nothing = LT
      sorter (Just x) (Just y) = compare x y
      
      --selects the definitions & the signatures of the functions with matching names
      matchingFuncNames = map (\(_,x,y) -> (x,y)) $ filter (\(fname,_,_) -> fname == name) library
      
      --selects that subset of matchingFuncNames whose elements
      --also match the arguments
      matchingFuncs = sortBy (\x y -> sorter ($(sel 3 3) x) ($(sel 3 3) y)) $
                      map (\(def,sig) -> (def, sig, sigDiff' (lazyArgs sig) sig))
                      matchingFuncNames
      
      (closestDef, closestSig, closestDiff) = head $ matchingFuncs
      castRes = castToSig (lazyArgs closestSig) closestSig
   in
      --Error 1: no function with this name found
      if (null matchingFuncNames) then throw (FNFException name (exprs2DT []) [])
         else if (length closestSig) /= (length args) then throw (FNFException name (exprs2DT (lazyArgs closestSig)) (map $(sel 2 2) matchingFuncNames))
         else case castRes of
              -- castable arguments -> apply function and pack the result into an appropriate return type
              -- Anothing reduction is applied here to resolve the casts.
              -- This also corresponds to eager evaluation, in case a lazy (unevaluated) argument was used.
              -- Ex: the chosen branch of if is reduced here
              Left castArgs -> (flip freduce) libs $ closestDef (map ((flip freduce) libs) castArgs)
              --error -> throw exception about uncastable arguments
              Right errPos -> if isInline then throw (CastException errPos closestSig)
                              else throw (FNFException name (exprs2DT (lazyArgs closestSig)) (map $(sel 2 2) matchingFuncNames))
   
